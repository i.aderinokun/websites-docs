# Create a new job ad on eyeo.com


1. [Add a new job](#add-a-new-job)
1. [Address review comments](#address-review-comments)
1. [Remove a job](#remove-a-job)
1. [Resolve conflicts](#resolve-conflicts)
1. [Edit a job](#edit-a-job)

## Add a new job

To add a new job you need to:

1. [Create a new branch](#create-a-new-branch)
1. [Add the job](#add-the-job)
1. [Add the job to header template](#add-the-job-to-header-template)
1. [Create a merge request](#create-a-merge-request)

### Create a new branch

Open <https://gitlab.com/eyeo/web.eyeo.com>.

Click the plus sign next to the repository name below the navigation and select *New branch*.

![](/res/docs/eyeo.com/create-job-ad/create-new-branch-1.png)

Enter a meaningful name. In this example we have a issue number and a job title, both are valueable information so *5609-student-help-monitoring* would be agood branch name. Click on the green *Create branch* button to continue.

***NOTE***: To learn more about branches see <https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell>

![](/res/docs/eyeo.com/create-job-ad/create-new-branch-2.png)

[Back to section beginning](#add-a-new-job)

### Add the job

Next we have to create a document for the new job add. The job ad must be placed into the folder */pages/job/* so first we have to navigate to the correct location.

Click on the *pages* directory.

![](/res/docs/eyeo.com/create-job-ad/add-job-1.png)

Click on the *jobs* directory.

![](/res/docs/eyeo.com/create-job-ad/add-job-2.png)

Click on the plus sign next to the path name below the main navigation and select *New file*.

![](/res/docs/eyeo.com/create-job-ad/add-job-3.png)

Choose and enter a new file name into the *File name* field.

![](/res/docs/eyeo.com/create-job-ad/add-job-4.png)

 Because the filename will become part of the url it should be the [slug form](https://en.wikipedia.org/wiki/Semantic_URL#Slug) of the title and because it is a markdown document it should have the *.md* file ending.
 
E.g. *Student Help Acceptable Ads Monitoring* becomes *student-help-acceptable-ads-monitoring.md*.

![](/res/docs/eyeo.com/create-job-ad/add-job-5.png)

Add the title as metdata *title=Student Help Acceptable Ads Monitoring*.

![](/res/docs/eyeo.com/create-job-ad/add-job-6.png)

Add the description as metadata *description=eyeo is looking for a University student based in Cologne to help monitor all things Adblock Plus.*.

![](/res/docs/eyeo.com/create-job-ad/add-job-7.png)

Include the header template by adding the line `<? include jobs/header ?>`

![](/res/docs/eyeo.com/create-job-ad/add-job-8.png)

Add the content of the job add.

![](/res/docs/eyeo.com/create-job-ad/add-job-9.png)

Include the footer template by adding the line `<? include jobs/footer ?>`

![](/res/docs/eyeo.com/create-job-ad/add-job-10.png)

Click the green *Commit changes* button below the form.

![](/res/docs/eyeo.com/create-job-ad/add-job-11.png)

Voila.

![](/res/docs/eyeo.com/create-job-ad/add-job-12.png)

[Back to section beginning](#add-a-new-job)

### Add the job to header template

Next we need to add the new job document to the job header template.

Click on the repository name *web.eyeo.com* below the main navigation.

![](/res/docs/eyeo.com/create-job-ad/add-job-header-1.png)

Click on the *includes* folder.

![](/res/docs/eyeo.com/create-job-ad/add-job-header-2.png)

Click on the *jobs* folder.

![](/res/docs/eyeo.com/create-job-ad/add-job-header-3.png)

Click on the header template *header.tmpl*

![](/res/docs/eyeo.com/create-job-ad/add-job-header-4.png)

Click the *Edit* button to the right above the file browser.

![](/res/docs/eyeo.com/create-job-ad/add-job-header-5.png)

Add a new line at the position the job show appear. In this case we want the job to appear between the *Whitelist Maintainer* and *Student Help Data Protection (m/w)* ad.

![](/res/docs/eyeo.com/create-job-ad/add-job-header-6.png)

To add the new job enter a comma separated list of the job title, the slug from above and the Talents Tracker ID. The list must be enclosed with parenthesis () and both the job title and slug but not the Talent Tracker ID must be in quotes.

![](/res/docs/eyeo.com/create-job-ad/add-job-header-7.png)

Click the green *Commit changes* button below the form.

![](/res/docs/eyeo.com/create-job-ad/add-job-header-8.png)

Voila.

![](/res/docs/eyeo.com/create-job-ad/add-job-header-9.png)

[Back to section beginning](#add-a-new-job)

### Create a merge request

Next we have to create a merge request. A merge request a way to request that your changes should be added to the *master* branch. It can be commented and updated multiple times in case further changes are neccesary. For more information about merge requests see [Merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/).

To request to merge your changes clicke the blue *Create merge request* button.

![](/res/docs/eyeo.com/create-job-ad/create-merge-request-1.png)

Now choose a suitable merge request title.

![](/res/docs/eyeo.com/create-job-ad/create-merge-request-2.png)

Because the merge request title will later be used as the commit message it should follow our commit message conventions:

- If you have a issue id the commit message should start with *Issue XXXX* otherwhise with *Noissue*
- The above prefix and the message should be seperated by a dash *Issue XXXX - message* or *Noissue - message*
- The message should describe what the change is all about
- The message should **not** end with a dot

In this case *Issue 5609 - Add student help acceptable ads monitoring job* would be a suitable merge request title.

![](/res/docs/eyeo.com/create-job-ad/create-merge-request-3.png)

Check both the *Remove source branch when merge request is accepted.* and *Squash commits when merge request is accepted.* checkbox at the bottom of the page.

![](/res/docs/eyeo.com/create-job-ad/create-merge-request-4.png)

Click the green *Submit merge request* button below the form.

![](/res/docs/eyeo.com/create-job-ad/create-merge-request-5.png)

Voila.

![](/res/docs/eyeo.com/create-job-ad/create-merge-request-6.png)

[Back to section beginning](#add-a-new-job)

## Address review comments

After the merge request has been submitted it will be reviewed. If further changes need to be made the reviewer will make comments detailing what needs to be changed.

To address the comments open the corresponding merge request.

![](/res/docs/eyeo.com/create-job-ad/address-review-comments-1.png)

Scroll through the page to get an overview of the comments.

![](/res/docs/eyeo.com/create-job-ad/address-review-comments-2.png)

To address individual comments click the *Changes* tab below the merge request title.

![](/res/docs/eyeo.com/create-job-ad/address-review-comments-3.png)

Scroll down to the comment you want to address. In this case the reviewer remarked that we should add a newline between the list and the next paragraph.

![](/res/docs/eyeo.com/create-job-ad/address-review-comments-4.png)

To address the comment scroll up and press the *Edit* button at the top of the file containing the comment.

![](/res/docs/eyeo.com/create-job-ad/address-review-comments-5.png)

Now you can edit the file again.
![](/res/docs/eyeo.com/create-job-ad/address-review-comments-6.png)

Add the requested newline.

![](/res/docs/eyeo.com/create-job-ad/address-review-comments-7.png)

And click on the green *Commit changes* button below the form.

![](/res/docs/eyeo.com/create-job-ad/address-review-comments-8.png)

Now the file has been changed and we can resolve the comment.

Click the *Resolve discussion* button below the comment to resolve the comment.

![](/res/docs/eyeo.com/create-job-ad/address-review-comments-9.png)

The comment is now resolved and the reviewer is notfied about it.

![](/res/docs/eyeo.com/create-job-ad/address-review-comments-10.png)

[Back to section beginning](#address-review-comments)

## Remove a job

To remove a job we need [create a new branch](#create-a-new-branch) to remove the job from the *header.tmpl* and delete the job document.

### Remove job from header template

Edit the file *includes/jobs/header.tmpl*

![](/res/docs/eyeo.com/create-job-ad/delete-job-header-1.png)

In this case we want to remove the *Whitelist Maintainer*.

![](/res/docs/eyeo.com/create-job-ad/delete-job-header-2.png)

Remove the corresponding line from the list of *openings*.

![](/res/docs/eyeo.com/create-job-ad/delete-job-header-3.png)

And click the green *Commit changes* button below the form.

![](/res/docs/eyeo.com/create-job-ad/delete-job-header-4.png)

[Back to section beginning](#remove-a-job)

### Remove job document

Open the job document */pages/jobs/whitelist-maintainer.md* and click the red *Delete* button above the file.

![](/res/docs/eyeo.com/create-job-ad/delete-job-1.png)

Click the red *Delete file* button below the form in the overlay.

![](/res/docs/eyeo.com/create-job-ad/delete-job-2.png)

Click the blue *Create merge request* button and create a merge request named *Noissue - Removed whitelist maintainer job*

![](/res/docs/eyeo.com/create-job-ad/delete-job-3.png)

[Back to section beginning](#remove-a-job)

## Resolve conflicts

When a file is edited concurrently there is no automatic way to decide which version is correct. Depending on what was changed you want to keep version A, B, a combination of both or even make further changes to accomodate the changes.

Whenever that happens merge request will be marked as not automatically mergeable.

![](/res/docs/eyeo.com/create-job-ad/resolve-conflict-1.png)

![](/res/docs/eyeo.com/create-job-ad/resolve-conflict-2.png)

We now see the two conflicting version.

On the left we see our version. Where our is relative to the merge request we opened.
On the right we see the version with wich our changes conflict.

In this case we see that a new line

`("Student Help Acceptable Ads Monitoring", "student-help-acceptable-ads-monitoring.md", 110),`

has been added below the line we want to remove

`("Whitelist Maintainer", "whitelist-maintainer", 108),`

We want to keep the new line but also remove the line we initially removed.

![](/res/docs/eyeo.com/create-job-ad/resolve-conflict-3.png)

This is not possible with the *Interactive mode* so click the *Edit iinline* button to switch to the manual mode.


![](/res/docs/eyeo.com/create-job-ad/resolve-conflict-4.png)

In the manual mode we see a editable version of the file that contains both version seperated by `=======` and confinded by `<<<<<<<` and `>>>>>>>`.

![](/res/docs/eyeo.com/create-job-ad/resolve-conflict-5.png)

We want to remove the

`("Whitelist Maintainer", "whitelist-maintainer", 108),`

because this pull request is supposed to remove the *Whitelist Maintainer* job.

![](/res/docs/eyeo.com/create-job-ad/resolve-conflict-6.png)

We want to keep the

`("Student Help Acceptable Ads Monitoring", "student-help-acceptable-ads-monitoring.md", 110),`

because it was added after we requested this merge request.

![](/res/docs/eyeo.com/create-job-ad/resolve-conflict-7.png)

Now we want to remove the conflict markers.

![](/res/docs/eyeo.com/create-job-ad/resolve-conflict-8.png)

Now we commit the result by clicking on the green *Commit conflict resolution* button below the form.

![](/res/docs/eyeo.com/create-job-ad/resolve-conflict-9.png)

Voila

![](/res/docs/eyeo.com/create-job-ad/resolve-conflict-10.png)

[Back to section beginning](#resolve-conflicts)

## Edit a job

By now you should be able to edit an existing job. To recap you neet to:

1. [Create a new branch](#create-a-new-branch) for your change
1. [Navigate](#add-the-job) to the job in the folder */pages/jobs/*
1. [Edit](#address-review-comments) the corresponding file.
1. [Create a merge request](#create-a-merge-request) for your changes
