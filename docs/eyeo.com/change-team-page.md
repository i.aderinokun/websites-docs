# Change the team page

1. [Add a new employee](#add-a-new-employee)
1. [Change existing employee](change-existing-employee)

## Add a new employee

1. [Create a new branch](/docs/eyeo.com/create-job-ad.md#create-a-new-branch)
1. [Upload employee photo](#upload-employee-photo)
1. [Add employee to globals.tmpl](#add-employee-to-globalstmpl)
1. [Create merge request](#create-merge-request)

### Upload employee photo

Navigate to to `/static/images/people.png`

![](/res/docs/eyeo.com/change-team-page/upload-employee-photo-1.png)
![](/res/docs/eyeo.com/change-team-page/upload-employee-photo-2.png)
![](/res/docs/eyeo.com/change-team-page/upload-employee-photo-3.png)


Click on the plus sign next to the path name below the main navigation and select *Upload file*.

![](/res/docs/eyeo.com/change-team-page/upload-employee-photo-4.png)

Click on the *click to upload* link to open the choose file dialog.

![](/res/docs/eyeo.com/change-team-page/upload-employee-photo-5.png)

Select the employee photo to upload.

***NOTE***: To remain consistency the file name should be *firstname.png*, in case of a duplicate use *firstname_lastname.png*.

![](/res/docs/eyeo.com/change-team-page/upload-employee-photo-6.png)

Click the green *Upload file* button below the form.

![](/res/docs/eyeo.com/change-team-page/upload-employee-photo-7.png)

[Back to section beginning](#add-a-new-employee)

### Add employee to globals.tmpl

Navigate to `/includes/`

![](/res/docs/eyeo.com/change-team-page/add-employee-to-globals-tmpl-1.png)
![](/res/docs/eyeo.com/change-team-page/add-employee-to-globals-tmpl-2.png)
![](/res/docs/eyeo.com/change-team-page/add-employee-to-globals-tmpl-3.png)


Click the *Edit* button to the right above the file.

![](/res/docs/eyeo.com/change-team-page/add-employee-to-globals-tmpl-4.png)

Scroll to the bottom of the *people* list.

![](/res/docs/eyeo.com/change-team-page/add-employee-to-globals-tmpl-5.png)

Add a new line

![](/res/docs/eyeo.com/change-team-page/add-employee-to-globals-tmpl-6.png)


Add a tuple that contains:

- The date the employee joined
- The country code for the flag, see [list of available flags](https://gitlab.com/eyeo/web.eyeo.com/tree/master/static/images/flags)
- The name of the employee photo you uploaded [above](#upload-employee-photo)
- The name of the employee
- The title of the employee
- The bio of the employee

Make sure to put everything in quotes and put a comma behind the closing parenthesis.
```
("2017-09-04", "scotland", "laura_faint.png", "Laura Faint", "Community Manager", "Always-curious, obsessed with learning new things, and happiest when bringing people together and facilitating meaningful connections."),
```

![](/res/docs/eyeo.com/change-team-page/add-employee-to-globals-tmpl-7.png)

Click the green *Commit changes* button below the form.

![](/res/docs/eyeo.com/change-team-page/add-employee-to-globals-tmpl-8.png)

Voila

![](/res/docs/eyeo.com/change-team-page/add-employee-to-globals-tmpl-9.png)

[Back to section beginning](#add-a-new-employee)

### Create merge request

Click the blue *Create merge request* to create a merge request for this changes as described in [create a merge request](/docs/eyeo.com/create-job-ad.md#create-a-merge-request)

![](/res/docs/eyeo.com/change-team-page/create-merge-request-1.png)

[Back to section beginning](#add-a-new-employee)

## Change existing employee

1. [Create a new branch](/docs/eyeo.com/create-job-ad.md#create-a-new-branch)
1. [Navigate to global.templ](#upload-employee-photo)
1. [Identify & change employee](#identify--change-employee)
1. [Create merge request](#create-merge-request)

### Identify & change employee

Click the *Edit* button to the right above the file.

![](/res/docs/eyeo.com/change-team-page/change-existing-employee-1.png)

Find the employee you want to change.

![](/res/docs/eyeo.com/change-team-page/change-existing-employee-2.png)

Make the neccesary changes.

![](/res/docs/eyeo.com/change-team-page/change-existing-employee-3.png)

Click the green *Commit changes* button below the form.

![](/res/docs/eyeo.com/change-team-page/change-existing-employee-4.png)

[Back to section beginning](#change-existing-employee)
